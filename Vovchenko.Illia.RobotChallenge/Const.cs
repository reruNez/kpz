﻿namespace Vovchenko.Illia.RobotChallenge
{
    public class Const
    {
        public static int CollectDistance { get; private set; } = 3;
        public static int EnergyToCreateRobot { get; private set; } = 50;
        public static int MinEnergyAfterCreate { get; private set; } = 100;
        public static int MaxRobotCount { get; private set; } = 100;
        public static int MaxRobotsOnStation { get; private set; } = 50;
        public static int TotalRounds { get; private set; } = 50;
        public static int MaxEnergyOnStation { get; private set; } = 1000;
        public static int MinEnergyTurn { get; private set; } = 50;
        public static int MaxEnergyTurn { get; private set; } = 100;
        public static int MaxCollectEnergyTurn { get; private set; } = 200;
        public static int AttackLoss { get; private set; } = 10;
    }
}
