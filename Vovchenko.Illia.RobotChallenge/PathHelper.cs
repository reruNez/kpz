﻿using Robot.Common;

namespace Vovchenko.Illia.RobotChallenge
{
    class PathHelper
    {
        static public Position PathCutter(Position destination, Position start, Robot.Common.Robot movingRobot)
        {
            var x = destination.X - start.X;
            var y = destination.Y - start.Y;
            int x_temp1;
            int x_temp2;
            int y_temp1;
            int y_temp2;
            Position temp1 = start;
            Position temp2;
            Position temp3;
            for (var i = 2; i < 50; i++)
            {
                x_temp1 = x / i;
                y_temp1 = y / i;
                x_temp2 = x % i;
                y_temp2 = y % i;
                temp2 = new Position(temp1.X + x_temp1, temp1.Y + y_temp1);
                temp3 = new Position(temp1.X + x_temp1 + x_temp2 , temp1.Y + y_temp1 +y_temp2);
                var result = DistanceHelper.FindDistance(temp2, temp1) * i-1 + DistanceHelper.FindDistance(temp3, temp2);
                if (result < movingRobot.Energy) return temp2; 
                temp2 = new Position(temp1.X + x_temp1, temp1.Y + y_temp1);
                temp3 = new Position(temp2.X + x_temp2, temp2.Y + y_temp2);
                result = DistanceHelper.FindDistance(temp2, temp1) * i+ DistanceHelper.FindDistance(temp3, temp2);
                if (result < movingRobot.Energy) return temp2; 
            }

            return null;
        }

    }
}
