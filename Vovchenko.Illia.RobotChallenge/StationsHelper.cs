﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Cryptography.X509Certificates;
using Robot.Common;

namespace Vovchenko.Illia.RobotChallenge
{
    public class StationsHelper
    {
        static public Position StationNearestCell(Robot.Common.Robot movingRobot, IList<Robot.Common.Robot> robots, EnergyStation station)
        {
            Position nearest = null;
            int minDistance = int.MaxValue;
            for (int i = station.Position.X - 2; i <= station.Position.X + 2; ++i)
            {
                for (int j = station.Position.Y - 2; j <= station.Position.Y + 2; ++j)
                {
                    Position cell = new Position(i, j);
                    if (IsCellFree(cell, movingRobot, robots))
                    {
                        int d = DistanceHelper.FindDistance(cell, movingRobot.Position);
                        if (d < minDistance)
                        {
                            minDistance = d;
                            nearest = cell;
                        }
                    }

                }

            }
            return nearest;
        }
        public static bool IsCellFree(Position cell, Robot.Common.Robot movingRobot, IList<Robot.Common.Robot> robots)
        {
            foreach (var robot in robots)
            {
                if (robot != movingRobot)
                {
                    if (robot.Position == cell)
                        return false;
                }
            }
            return true;
        }

        static public Position FindNearestStationCell(Robot.Common.Robot movingRobot, Map map,
            IList<Robot.Common.Robot> robots, IList<EnergyStation> stations)
        {
            Position nearestcell = null;
            EnergyStation nearest = FindNearestStation(movingRobot, map);
            EnergyStation at = FindNearestStation(movingRobot, map);
            int minDistance = Int32.MaxValue-1;
            foreach (var station in stations)
            {
                Position nearCell;
                if (IsCellFree(station.Position, movingRobot, robots))
                    nearCell = station.Position;
                else
                    nearCell = StationNearestCell(movingRobot, robots, station);
                if (nearCell != null)
                {
                    int d = DistanceHelper.FindDistance(nearCell, movingRobot.Position);
                    if (d < minDistance)
                    {
                        if (IsWorth(nearest, station, at, movingRobot))
                        {
                                    
                            minDistance = d;
                            nearestcell = nearCell;
                            nearest = station;

                        }
                    }
                    else if (d == minDistance)
                    {
                        if (station.Energy > nearest.Energy)
                        {
                            if (IsWorth(nearest, station, at, movingRobot))
                            {
                                minDistance = d;
                                nearestcell = nearCell;
                                nearest = station;
                            }
                        }
                    }

                }
            }
            return nearestcell;
        }

        static public EnergyStation FindNearestStation(Robot.Common.Robot movingRobot, Map map)
        {
            int minDistance = int.MaxValue - 1;
            EnergyStation nearest = null;
            foreach (var station in map.Stations)
            {
                int d = DistanceHelper.FindDistance(station.Position, movingRobot.Position);
                if (d < minDistance)
                {
                    minDistance = d;
                    nearest = station;
                }
            }
            return nearest;
        }

        static public bool StationCheck(Robot.Common.Robot movingRobot, EnergyStation station, Map map, IList<Robot.Common.Robot> robots, int maxbots)
        {
            int botCount = 0;
            for (int i = station.Position.X - 1;
                i <= station.Position.X + 1;
                ++i)
            {
                for (int j = station.Position.Y + 1; j >= station.Position.Y - 1; --j)
                {
                    Position cell = new Position(i, j);
                    if (!IsCellFree(cell, movingRobot, robots))
                    {
                        foreach (var robot in robots)
                        {
                            if (robot.Position == cell && robot != movingRobot && FindNearestStation(robot, map) == station)
                            {
                                botCount++;
                            }
                        }
                    }
                }

            }
            if (botCount <= maxbots) return true;
            return false;
        }

        static public IList<EnergyStation> NiceStations(Map map, Robot.Common.Robot movingRobot, IList<Robot.Common.Robot> robots, int maxbots)
        {
            return map.Stations.Where(e => StationCheck(movingRobot, e, map, robots, maxbots)).ToList();
        }

        static public bool IsWorth(EnergyStation station1, EnergyStation station2, EnergyStation station3, Robot.Common.Robot movingRobot)
        {
            if (movingRobot.Energy >= DistanceHelper.FindDistance(movingRobot.Position, station2.Position))
                return true;
            return false;
        }

    }
}
