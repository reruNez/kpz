﻿using System.Collections.Generic;
using System.Linq;
using Robot.Common;
using System.IO;

namespace Vovchenko.Illia.RobotChallenge
{

    public class VovchenkoAlgorithm : IRobotAlgorithm
    {


        public  RobotCommand DoStep(IList<Robot.Common.Robot> robots, int robotToMoveIndex, Map map)
        {
            var movingRobot = robots[robotToMoveIndex];
            if (MyRobots(robots).Count() < 100 && movingRobot.Energy >= 250)
            {
                return new CreateNewRobotCommand(){NewRobotEnergy = 200};
            }

            int maxbots = 1;
            IList<EnergyStation> stations1 = StationsHelper.NiceStations(map, movingRobot, robots, maxbots);
            IList<EnergyStation> stations3 = map.Stations;
            Position stationPosition = StationsHelper.FindNearestStationCell(movingRobot, map, robots, stations1);
            if (stationPosition == null)
            {
                while (stationPosition == null && maxbots < 50)
                {
                    stations1 = StationsHelper.NiceStations(map, movingRobot, robots, maxbots);
                    stationPosition = StationsHelper.FindNearestStationCell(movingRobot, map, robots, stations1);
                    maxbots++;
                }
                if (stationPosition == null)
                    stationPosition = StationsHelper.FindNearestStationCell(movingRobot, map, robots, stations3);
            }

            if (stationPosition != null)
            {
                if (stationPosition == movingRobot.Position)
                    return new CollectEnergyCommand();
                return new MoveCommand() { NewPosition = stationPosition };
            }
            return null;   
        }   

        private bool IsMyRobot(Robot.Common.Robot robot)
        {
            return robot.OwnerName == Author;
        }

        public IList<Robot.Common.Robot> MyRobots(IList<Robot.Common.Robot> robots)
        {
            return robots.Where(r => IsMyRobot(r)).ToList();
        }



        public string Author
        {
            get { return "Vovchenko Illia"; }
        }

    }
}
