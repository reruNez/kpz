﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Runtime.InteropServices.ComTypes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Robot.Common;
using Vovchenko.Illia.RobotChallenge;

namespace VovchenkoChallenge.Test
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestCollectStandingOnStation()
        {
            Map m = new Map();
            m.Stations.Add(new EnergyStation() { Position = new Position(1,1), Energy = 100});
            var robots = new List<Robot.Common.Robot>();
            robots.Add(new Robot.Common.Robot() { Position = new Position(1, 1), Energy = 100});

            var c = new VovchenkoAlgorithm();
            var b = c.DoStep(robots, 0, m);
            Assert.IsTrue(b is CollectEnergyCommand);
        }

        [TestMethod]
        public void TestNewRobotCommand()
        {
            Map m = new Map();
            m.Stations.Add(new EnergyStation() { Position = new Position(1, 1), Energy = 100 });
            var robots = new List<Robot.Common.Robot>();
            robots.Add(new Robot.Common.Robot() { Position = new Position(1, 1), Energy = 200 });

            var c = new VovchenkoAlgorithm();
            var b = c.DoStep(robots, 0, m);
            Assert.IsTrue(b is CreateNewRobotCommand);
        }

        [TestMethod]
        public void TestMovingCommand()
        {
            Map m = new Map();
            m.Stations.Add(new EnergyStation() { Position = new Position(4, 4), Energy = 100 });
            var robots = new List<Robot.Common.Robot>();
            robots.Add(new Robot.Common.Robot() { Position = new Position(1, 1), Energy = 100 });
            var c = new VovchenkoAlgorithm();
            var b = c.DoStep(robots, 0, m);
            Assert.IsTrue(b is MoveCommand);
        }


        [TestMethod]
        public void TestFreeCell()
        {
            Map m = new Map();
            var robots = new List<Robot.Common.Robot>();
            robots.Add(new Robot.Common.Robot() { Position = new Position(1, 1), Energy = 1000 });
            robots.Add(new Robot.Common.Robot() { Position = new Position(3, 3), Energy = 1000 });
            var b = StationsHelper.IsCellFree(robots[0].Position, robots[1], robots);
            Assert.IsTrue(b is false);
        }

        [TestMethod]
        public void TestStationBotCheck()
        {
            Map m = new Map();
            m.Stations.Add(new EnergyStation() { Position = new Position(1, 1), Energy = 100 });
            var robots = new List<Robot.Common.Robot>();
            var maxbots = 2;
            robots.Add(new Robot.Common.Robot() { Position = new Position(1, 1), Energy = 1000 });
            robots.Add(new Robot.Common.Robot() { Position = new Position(2, 2), Energy = 1000 });
            robots.Add(new Robot.Common.Robot() { Position = new Position(3, 3), Energy = 1000 });
            var b = StationsHelper.StationCheck(robots[0], m.Stations[0], m, robots, maxbots);
            Assert.AreEqual(true, b);
        }

        [TestMethod]
        public void TestStationBotCheck2()
        {
            Map m = new Map();
            m.Stations.Add(new EnergyStation() { Position = new Position(1, 1), Energy = 100 });
            var robots = new List<Robot.Common.Robot>();
            var maxbots = 2;
            robots.Add(new Robot.Common.Robot() { Position = new Position(10, 10), Energy = 1000 });
            robots.Add(new Robot.Common.Robot() { Position = new Position(2, 2), Energy = 1000 });
            robots.Add(new Robot.Common.Robot() { Position = new Position(3, 3), Energy = 1000 });
            var b = StationsHelper.StationCheck(robots[0], m.Stations[0], m, robots, maxbots);
            Assert.AreEqual(true, b);
        }


        [TestMethod]
        public void TestNearestStation()
        {
            Map m = new Map();
            m.Stations.Add(new EnergyStation() { Position = new Position(1, 1), Energy = 100 });
            m.Stations.Add(new EnergyStation() { Position = new Position(6, 6), Energy = 100 });
            var robots = new List<Robot.Common.Robot>();
            robots.Add(new Robot.Common.Robot() { Position = new Position(4, 4), Energy = 1000 });
            var b = StationsHelper.FindNearestStation(robots[0], m);
            Assert.AreEqual( m.Stations[1], b);
        }

        [TestMethod]
        public void TestStationNearestCell()
        {
            Map m = new Map();
            m.Stations.Add(new EnergyStation() { Position = new Position(6, 6), Energy = 100 });
            var robots = new List<Robot.Common.Robot>();
            robots.Add(new Robot.Common.Robot() { Position = new Position(11, 6), Energy = 1000 });
            robots.Add(new Robot.Common.Robot() { Position = new Position(7, 6), Energy = 1000 });
            var expect = new Position(8, 6);
            var b = StationsHelper.StationNearestCell(robots[0], robots, m.Stations[0]);
            Assert.AreEqual(expect , b);
        }

        [TestMethod]
        public void TestIsEnough()
        {
            Map m = new Map();
            m.Stations.Add(new EnergyStation() { Position = new Position(6, 6), Energy = 100 });
            m.Stations.Add(new EnergyStation() { Position = new Position(7, 7), Energy = 100 });
            var robots = new List<Robot.Common.Robot>();
            robots.Add(new Robot.Common.Robot() { Position = new Position(11, 6), Energy = 300 });
            var expect = new Position(8, 6);
            var b = StationsHelper.IsWorth(m.Stations[0], m.Stations[1], m.Stations[1], robots[0]);
            Assert.IsTrue(b);
        }

        [TestMethod]
        public void TestFindNearestStationCell()
        {
            Map m = new Map();
            m.Stations.Add(new EnergyStation() { Position = new Position(8, 6), Energy = 100 });
            m.Stations.Add(new EnergyStation() { Position = new Position(10, 5), Energy = 100 });
            m.Stations.Add(new EnergyStation() { Position = new Position(12, 6), Energy = 200 });
            var robots = new List<Robot.Common.Robot>();
            robots.Add(new Robot.Common.Robot() { Position = new Position(10, 6), Energy = 1000 });
            robots.Add(new Robot.Common.Robot() { Position = new Position(12, 6), Energy = 1000 });
            robots.Add(new Robot.Common.Robot() { Position = new Position(10, 10), Energy = 1000 });
            var expect = new Position(10, 8);
            var b = StationsHelper.FindNearestStationCell(robots[2], m, robots, m.Stations);
            Assert.AreEqual(expect, b);
        }
    }
}
